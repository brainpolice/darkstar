import Config

# config(:libcluster,
#   debug: true
# )

config(:reactor,
  repo_adapter:
    case config_env() do
      :prod -> Ecto.Adapters.Postgres
      _ -> Ecto.Adapters.SQLite3
    end
)

config(:reactor, ecto_repos: [Reactor.Storage])
