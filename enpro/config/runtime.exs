import Config

config(:reactor, ClusterTopologies,
  mars_city:
    case config_env() do
      :prod ->
        [
          strategy: Elixir.Cluster.Strategy.Epmd,
          config: [
            timeout: 5_000,
            hosts:
              System.fetch_env!("HOSTS")
              |> String.split(",")
              |> Enum.map(&String.to_atom(&1))
          ]
        ]

      _ ->
        [
          strategy: Elixir.Cluster.Strategy.Gossip,
          config: [
            port: 45892,
            if_addr: "0.0.0.0",
            multicast_addr: "255.255.255.255",
            broadcast_only: true
          ]
        ]
    end
)

config(:amqp,
  connections: [
    comsfacility: [
      url:
        case config_env() do
          :prod -> System.fetch_env!("COMSFACILITY")
          _ -> "amqp://guest:guest@127.0.0.1"
        end
    ]
  ],
  channels: [
    ch0: [connection: :comsfacility]
  ]
)

config(
  :reactor,
  Reactor.Storage,
  case config_env() do
    :prod ->
      [
        database: "fatzombie",
        username: "fatzombie",
        password: "mmmboy",
        hostname: System.fetch_env!("FATZOMBIE"),
        port: "5432"
      ]

    _ ->
      [
        database: "./db.sqlite"
      ]
  end
)
