defmodule Reactor do
  @moduledoc """
  Documentation for `Reactor`.
  """

  # todo this would not scale well
  def start_accounts() do
    for account <- Reactor.Query.accounts() do
      Reactor.AccountSup.start_supervised(account)
    end
  end

  # todo this is dumb
  @spec via(atom(), atom(), atom(), any()) :: tuple()
  defp via(reg_mod, reg_name, mod, id) do
    key = apply(mod, :registry_key, [id])
    {:via, reg_mod, {reg_name, key}}
  end

  @spec lookup(atom(), atom(), atom(), any()) :: list()
  defp lookup(reg_mod, reg_name, mod, id) do
    key = apply(mod, :registry_key, [id])
    apply(reg_mod, :lookup, [reg_name, key])
  end

  @spec lookup_pid(atom(), atom(), atom(), any()) :: pid()
  defp lookup_pid(reg_mod, reg_name, mod, id) do
    [{pid, _}] = lookup(reg_mod, reg_name, mod, id)
    pid
  end

  @spec hr_via(atom(), any()) :: tuple()
  def hr_via(mod, id) do
    via(Horde.Registry, Reactor.HR, mod, id)
  end

  # @spec reactor_lookup(atom(), any()) :: list()
  # def reactor_lookup(mod, id) do
  #   lookup(Horde.Registry, Reactor.HR, mod, id)
  # end

  @spec hr_lookup_pid(atom(), any()) :: pid()
  def hr_lookup_pid(mod, id) do
    lookup_pid(Horde.Registry, Reactor.HR, mod, id)
  end
end
