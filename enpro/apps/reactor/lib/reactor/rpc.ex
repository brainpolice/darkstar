defmodule Reactor.ControlRoom do
  use Supervisor

  def start_link(_arg) do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @impl true
  def init(_init_arg) do
    children = [
      {Reactor.Intercom, []}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end

defmodule Reactor.Intercom do
  use GenServer
  use AMQP

  def start_link(_arg) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @impl true
  def init(_init_arg) do
    Process.flag(:trap_exit, true)

    {:ok, chan} = AMQP.Application.get_channel(:ch0)
    {:ok, %{queue: queue}} = Queue.declare(chan, "Reactor.Intercom")
    :ok = Basic.qos(chan, prefetch_count: 1)
    {:ok, ctag} = Basic.consume(chan, queue)

    {:ok, %{chan: chan, ctag: ctag}}
  end

  @impl true
  def handle_info({:basic_consume_ok, _}, state) do
    {:noreply, state}
  end

  @impl true
  def handle_info({:basic_deliver, msg, meta}, state) do
    :ok = Basic.ack(state.chan, meta.delivery_tag)
    Reactor.Transmission.start_supervised({msg, meta})

    {:noreply, state}
  end
end

defmodule Reactor.Transmission do
  use Task
  use AMQP
  import Ecto.Query, only: [from: 2]

  def start_link(arg) do
    Task.start_link(__MODULE__, :run, [arg])
  end

  def start_supervised(arg) do
    Horde.DynamicSupervisor.start_child(Reactor.HDS, {__MODULE__, arg})
  end

  defp create_account(%{account: account_to_create}) do
    insert_result =
      Reactor.Storage.Account.changeset(%Reactor.Storage.Account{}, %{
        username: account_to_create.username,
        password: Bcrypt.hash_pwd_salt(account_to_create.password)
      })
      |> Reactor.Storage.insert()

    case insert_result do
      {:ok, created_account} ->
        {:ok, _} = Reactor.AccountSup.start_supervised(created_account)

        %{
          status: "ok",
          account: created_account
        }

      {:error, changeset} ->
        raise Kernel.inspect(changeset.errors)
    end
  end

  defp delete_account(%{account: account_to_delete}) do
    # todo move to AccountSup?
    :ok =
      Horde.DynamicSupervisor.terminate_child(
        Reactor.HDS,
        Reactor.hr_lookup_pid(Reactor.AccountSup, account_to_delete.id)
      )

    {1, _} =
      from(a in Reactor.Storage.Account, where: a.id == ^account_to_delete.id)
      |> Reactor.Storage.delete_all()

    %{status: "ok"}
  end

  defp create_service(%{service: service}) do
    {:ok, service} = Reactor.Account.create_service(service)

    %{
      status: "ok",
      service: service
    }
  end

  def run({msg, meta}) do
    res =
      try do
        decoded_msg = Jason.decode!(msg, keys: :atoms)

        case decoded_msg.type do
          "create_account" ->
            create_account(decoded_msg)

          "delete_account" ->
            delete_account(decoded_msg)

          "create_service" ->
            create_service(decoded_msg)
        end
      rescue
        e ->
          %{
            status: "error",
            error: Exception.format(:error, e)
          }
      end

    json =
      case Jason.encode(res) do
        {:ok, s} ->
          s

        {:error, e} ->
          Jason.encode!(%{
            status: "error",
            error: Exception.format(:error, e)
          })
      end

    {:ok, chan} = AMQP.Application.get_channel(:ch0)

    :ok =
      Basic.publish(
        chan,
        "",
        meta.reply_to,
        json,
        correlation_id: meta.correlation_id
      )
  end
end

defmodule Reactor.RPC do
  use AMQP

  @spec call(String.t(), String.t()) :: {:ok, String.t()}
  defp call(req_queue, msg) do
    {:ok, chan} = AMQP.Application.get_channel(:ch0)
    {:ok, %{queue: res_queue}} = Queue.declare(chan, "", exclusive: true)
    {:ok, _} = Basic.consume(chan, res_queue, nil, no_ack: true)

    correlation_id =
      :erlang.unique_integer()
      |> :erlang.integer_to_binary()
      |> Base.encode64()

    :ok =
      Basic.publish(
        chan,
        "",
        req_queue,
        msg,
        reply_to: res_queue,
        correlation_id: correlation_id
      )

    receive do
      {:basic_deliver, res, %{correlation_id: ^correlation_id}} ->
        {:ok, res}
        # todo
        # _ ->
        #   {:error, msg}
    end
  end

  def hw() do
    # msg = Jason.encode!(%{type: "create_account", account: account})
    call("Reactor.Intercom", "hello")
  end

  @spec create_account(%Reactor.Storage.Account{}) :: {:ok, String.t()}
  def create_account(account) do
    msg = Jason.encode!(%{type: "create_account", account: account})
    call("Reactor.Intercom", msg)
  end

  @spec delete_account(%Reactor.Storage.Account{}) :: {:ok, String.t()}
  def delete_account(account) do
    {:ok, msg} = Jason.encode(%{type: "delete_account", account: account})
    call("Reactor.Intercom", msg)
  end

  @spec create_service(%Reactor.Storage.Service{}) :: {:ok, String.t()}
  def create_service(service) do
    {:ok, msg} = Jason.encode(%{type: "create_service", service: service})
    call("Reactor.Intercom", msg)
  end
end
