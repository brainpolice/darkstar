defmodule Reactor.AccountSup do
  use Supervisor

  @spec start_link(any()) :: Supervisor.on_start()
  def start_link(account) do
    name = Reactor.hr_via(__MODULE__, account.id)
    Supervisor.start_link(__MODULE__, account, name: name)
  end

  @spec registry_key(any()) :: String.t()
  def registry_key(account_id) do
    "#account_sup$account_id:#{account_id}"
  end

  @spec start_supervised(%Reactor.Storage.Account{}) :: DynamicSupervisor.on_start_child()
  def start_supervised(account) do
    Horde.DynamicSupervisor.start_child(Reactor.HDS, {Reactor.AccountSup, account})
  end

  @impl true
  def init(account) do
    children = [
      {Reactor.ServicesDynSup, account},
      {Reactor.Account, account}
    ]

    Supervisor.init(children, strategy: :rest_for_one)
  end
end

defmodule Reactor.Account do
  use GenServer

  def start_link(account) do
    name = Reactor.hr_via(__MODULE__, account.id)
    GenServer.start_link(__MODULE__, account, name: name)
  end

  @spec registry_key(any()) :: String.t()
  def registry_key(account_id) do
    "#account$account_id:#{account_id}"
  end

  @spec create_service(%Reactor.Storage.Service{}) :: {:ok, %Reactor.Storage.Service{}}
  def create_service(service) do
    GenServer.call(
      Reactor.hr_lookup_pid(Reactor.Account, service.account_id),
      {:create_service, service}
    )
  end

  # todo optional service argument
  @impl true
  def init(account) do
    if is_list(account.services) do
      for service <- account.services do
        {:ok, _} =
          Reactor.Service.start_supervised(%{
            account: account,
            service: service
          })
      end
    end

    {:ok, %{account: account}}
  end

  @impl true
  def handle_call({:create_service, service}, _from, %{account: account} = state) do
    insert_result =
      Reactor.Storage.Service.changeset(
        %Reactor.Storage.Service{},
        %{
          name: service.name,
          protocol: service.protocol,
          account_id: service.account_id
        }
      )
      |> Reactor.Storage.insert()

    reply =
      case insert_result do
        {:ok, created_service} ->
          {:ok, _} =
            Reactor.Service.start_supervised(%{
              account: account,
              service: created_service
            })

          {:ok, created_service}

        {:error, changeset} ->
          {:error, changeset.errors}
      end

    {:reply, reply, state}
  end
end
