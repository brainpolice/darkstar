defmodule Reactor.Storage do
  use Ecto.Repo,
    otp_app: :reactor,
    adapter: Application.compile_env!(:reactor, :repo_adapter)
end

# todo cascade

defmodule Reactor.Storage.Account do
  use Ecto.Schema
  import Ecto.Changeset

  # todo ignore metadata
  @derive {Jason.Encoder, only: [:id, :username, :password]}
  schema "account" do
    field(:username)
    field(:password)
    has_many(:services, Reactor.Storage.Service)
    has_many(:groups, Reactor.Storage.Group)
  end

  def changeset(account, params \\ %{}) do
    account
    |> cast(params, [:username, :password])
    |> validate_required([:username, :password])
    |> unique_constraint(:username)
  end
end

defmodule Reactor.Storage.Service do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :name, :protocol, :account_id]}
  schema "service" do
    field(:name)
    field(:protocol)
    belongs_to(:account, Reactor.Storage.Account)
    has_one(:service_irc, Reactor.Storage.ServiceIRC)
    has_one(:service_discord, Reactor.Storage.ServiceDiscord)
    has_one(:service_matrix, Reactor.Storage.ServiceMatrix)
    has_many(:rooms, Reactor.Storage.Room)
  end

  def changeset(service, params \\ %{}) do
    service
    |> cast(params, [:name, :protocol, :account_id])
    |> validate_required([:name, :protocol, :account_id])
    |> unique_constraint([:account_id, :name])
  end
end

defmodule Reactor.Storage.ServiceIRC do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :server, :port, :nick, :service_id]}
  schema "service_irc" do
    field(:server)
    field(:port, :integer)
    field(:nick)
    belongs_to(:service, Reactor.Storage.Service)
  end

  def changeset(service, params \\ %{}) do
    service
    |> cast(params, [:server, :port, :nick, :service_id])
    |> validate_required([:server, :port, :nick, :service_id])

    # |> unique_constraint([:account_id, :name])
  end
end

defmodule Reactor.Storage.ServiceDiscord do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :auth_token, :service_id]}
  schema "service_discord" do
    field(:auth_token)
    belongs_to(:service, Reactor.Storage.Service)
  end

  def changeset(service, params \\ %{}) do
    service
    |> cast(params, [:auth_token, :service_id])
    |> validate_required([:auth_token, :service_id])
  end
end

defmodule Reactor.Storage.ServiceMatrix do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :sender, :server, :access_token, :port, :service_id]}
  schema "service_matrix" do
    field(:sender)
    field(:server)
    field(:access_token)
    field(:port, :integer)
    belongs_to(:service, Reactor.Storage.Service)
  end

  def changeset(service, params \\ %{}) do
    service
    |> cast(params, [:sender, :server, :access_token, :port, :service_id])
    |> validate_required([:sender, :server, :access_token, :port, :service_id])
  end
end

###

defmodule Reactor.Storage.Room do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder,
           only: [:id, :name, :service_id, :group_id, :room_irc, :room_discord, :room_matrix]}
  schema "room" do
    field(:name)
    belongs_to(:service, Reactor.Storage.Service)
    belongs_to(:group, Reactor.Storage.Group)
    has_one(:room_irc, Reactor.Storage.RoomIRC)
    has_one(:room_discord, Reactor.Storage.RoomDiscord)
    has_one(:room_matrix, Reactor.Storage.RoomMatrix)
  end

  def changeset(service, params \\ %{}) do
    service
    |> cast(params, [:name, :service_id, :group_id])
    |> validate_required([:name, :service_id])
  end
end

defmodule Reactor.Storage.Group do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :name, :visibility, :account_id, :rooms]}
  schema "group" do
    field(:name)
    field(:visibility, :string, default: "private")
    belongs_to(:account, Reactor.Storage.Account)
    has_many(:rooms, Reactor.Storage.Room)
  end

  def changeset(group, params \\ %{}) do
    group
    |> cast(params, [:name, :visibility, :account_id])
    |> validate_required([:name, :visibility, :account_id])
  end
end

defmodule Reactor.Storage.RoomIRC do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :channel, :room_id]}
  schema "room_irc" do
    field(:channel)
    belongs_to(:room, Reactor.Storage.Service)
  end

  def changeset(service, params \\ %{}) do
    service
    |> cast(params, [:channel, :room_id])
    |> validate_required([:channel, :room_id])
  end
end

defmodule Reactor.Storage.RoomDiscord do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :channel_id, :room_id]}
  schema "room_discord" do
    field(:channel_id)
    belongs_to(:room, Reactor.Storage.Service)
  end

  def changeset(service, params \\ %{}) do
    service
    |> cast(params, [:channel_id, :room_id])
    |> validate_required([:channel_id, :room_id])
  end
end

defmodule Reactor.Storage.RoomMatrix do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :matrix_room_id, :room_id]}
  schema "room_matrix" do
    field(:matrix_room_id)
    belongs_to(:room, Reactor.Storage.Service)
  end

  def changeset(service, params \\ %{}) do
    service
    |> cast(params, [:matrix_room_id, :room_id])
    |> validate_required([:matrix_room_id, :room_id])
  end
end

defmodule Reactor.Query do
  import Ecto.Query, only: [from: 2]

  def accounts() do
    from(a in Reactor.Storage.Account,
      left_join: s in assoc(a, :services),
      left_join: si in assoc(s, :service_irc),
      left_join: sd in assoc(s, :service_discord),
      left_join: sm in assoc(s, :service_matrix),
      left_join: r in assoc(s, :rooms),
      left_join: rg in assoc(r, :group),
      left_join: ri in assoc(r, :room_irc),
      left_join: rd in assoc(r, :room_discord),
      left_join: rm in assoc(r, :room_matrix),
      preload: [
        services:
          {s,
           [
             service_irc: si,
             service_discord: sd,
             service_matrix: sm,
             rooms:
               {r,
                [
                  group: rg,
                  room_irc: ri,
                  room_discord: rd,
                  room_matrix: rm
                ]}
           ]}
      ]
      # select: a
    )
    |> Reactor.Storage.all()
  end
end
