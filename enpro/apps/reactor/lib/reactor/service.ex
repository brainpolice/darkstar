defmodule Reactor.ServicesDynSup do
  use DynamicSupervisor

  @spec registry_key(any()) :: String.t()
  def registry_key(account_id) do
    "#services_dynsup$account_id:#{account_id}"
  end

  def start_link(account) do
    name = Reactor.hr_via(__MODULE__, account.id)
    DynamicSupervisor.start_link(__MODULE__, [], name: name)
  end

  @impl true
  def init(_init_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end

defmodule Reactor.Service do
  use GenServer
  use AMQP

  def registry_key(service_id) do
    "#service$service_id:#{service_id}"
  end

  def start_link(arg) do
    name = Reactor.hr_via(__MODULE__, arg.service.id)
    GenServer.start_link(__MODULE__, arg, name: name)
  end

  def start_supervised(arg) do
    DynamicSupervisor.start_child(
      Reactor.hr_lookup_pid(Reactor.ServicesDynSup, arg.account.id),
      {__MODULE__, arg}
    )
  end

  # plugin

  def mmmboy(service_id) do
    GenServer.call(
      Reactor.hr_lookup_pid(Reactor.Service, service_id),
      {:call_process, "mmmboy", %{}},
      15000
    )
  end

  def new_message(pid, msg) do
    GenServer.cast(pid, {:cast_process, "new_message", msg})
  end

  def register_rooms(service_id, rooms) do
    pid = Reactor.hr_lookup_pid(Reactor.Service, service_id)
    GenServer.call(pid, {:call_process, "register_rooms", %{rooms: rooms}})
  end

  def register_rooms_async(pid, rooms) do
    GenServer.cast(pid, {:cast_process, "register_rooms_async", %{rooms: rooms}})
  end

  @impl true
  def init(init_arg) do
    Process.flag(:trap_exit, true)

    # amqp

    {:ok, ch} = AMQP.Application.get_channel(:ch0)

    {:ok, %{queue: q}} =
      Queue.declare(ch, "#service$service_id:#{init_arg.service.id}", durable: true)

    for room <- init_arg.service.rooms do
      if is_integer(room.group_id) do
        :ok =
          Queue.bind(ch, q, "reactor.messages", routing_key: "group_id:#{room.group_id}")
      end
    end

    {:ok, _} = Basic.consume(ch, q)

    # ports

    cmd =
      case init_arg.service.protocol do
        "irc" -> "swann"
        "discord" -> "campbell -auth_token=#{init_arg.service.service_discord.auth_token}"
        "matrix" -> "swann"
      end

    # todo link to executable
    # todo :spawn_exec w :args opt?
    port = Port.open({:spawn, cmd}, [:binary])

    state =
      Map.merge(init_arg, %{
        channel: ch,
        port: port
      })

    {:ok, state, {:continue, :post_init}}
  end

  @impl true
  def handle_call({:call_process, handler, args_map}, from, state) do
    ref =
      :erlang.unique_integer()
      |> :erlang.integer_to_binary()
      |> Base.encode64()

    j = Jason.encode!(Map.merge(%{handler: handler, ref: ref}, args_map))
    send(state.port, {self(), {:command, "#{j}\n"}})
    {:noreply, Map.merge(state, %{awaiting_reply: %{ref => from}})}
  end

  @impl true
  def handle_cast({:cast_process, handler, args_map}, state) do
    j = Jason.encode!(Map.merge(%{handler: handler}, args_map))
    send(state.port, {self(), {:command, "#{j}\n"}})
    {:noreply, state}
  end

  @impl true
  def handle_info({:basic_consume_ok, _}, state) do
    {:noreply, state}
  end

  @impl true
  def handle_info({:basic_deliver, data, meta}, state) do
    decoded = :erlang.binary_to_term(data)
    {:noreply, state, {:continue, {:handle_basic_deliver, data, meta, decoded}}}
  end

  @impl true
  def handle_info({port, {:data, data}}, %{port: port} = state) do
    decoded = Jason.decode!(data, keys: :atoms)
    {:noreply, state, {:continue, {:handle_port, data, decoded}}}
  end

  @impl true
  def handle_info({:EXIT, port, _reason}, %{port: port} = state) do
    IO.puts("restarting due to plugin error")
    {:stop, :plugin_error, state}
  end

  ### init

  def handle_continue(:post_init, state) do
    IO.puts("post init")
    # __MODULE__.register_rooms_async(self(), state.service.rooms)
    {:noreply, state}
  end

  ### amqp

  @impl true
  def handle_continue(
        {:handle_basic_deliver, _data, meta, %{handler: "new_message"} = decoded},
        state
      ) do
    IO.puts("handle basic deliver, new message")
    IO.inspect(decoded)
    # IO.inspect(meta)

    [_, group_id_str] = String.split(meta.routing_key, ":")
    {group_id, _} = Integer.parse(group_id_str)
    # IO.inspect(group_id)

    rooms_in_group =
      Enum.filter(state.service.rooms, fn room ->
        if room.id == decoded.room_id do
          false
        else
          is_integer(room.group_id)
          room.group_id == group_id
        end

        # if is_integer(room.group_id) do
        #   room.group_id == group_id
        # end
      end)
      |> Enum.map(fn room ->
        case state.service.protocol do
          "irc" -> room.room_irc.channel
          "discord" -> room.room_discord.channel_id
          "matrix" -> room.room_matrix.matrix_room_id
        end
      end)

    if Enum.count(rooms_in_group) > 0 do
      __MODULE__.new_message(self(), Map.put(decoded, :room_refs, rooms_in_group))
    end

    {:noreply, state, {:continue, {:handle_basic_ack, meta}}}
  end

  @impl true
  def handle_continue({:handle_basic_ack, meta}, state) do
    IO.puts("acking #{meta.delivery_tag}")
    :ok = Basic.ack(state.channel, meta.delivery_tag)
    {:noreply, state}
  end

  ### ports

  @impl true
  def handle_continue({:handle_port, _data, %{handler: "reply", ref: ref} = decoded}, state) do
    IO.puts("handle port, reply")
    IO.inspect(decoded)
    {from, new_awaiting_reply} = Map.pop!(state.awaiting_reply, ref)
    :ok = GenServer.reply(from, decoded)
    {:noreply, Map.put(state, :awaiting_reply, new_awaiting_reply)}
  end

  @impl true
  def handle_continue({:handle_port, _data, %{handler: "new_message"} = decoded}, state) do
    IO.puts("handle port, new message")
    IO.inspect(decoded)

    room =
      Enum.find(state.service.rooms, fn room ->
        decoded.room_ref ==
          case state.service.protocol do
            "irc" -> room.room_irc.channel
            "discord" -> room.room_discord.channel_id
            "matrix" -> room.room_matrix.matrix_room_id
          end
      end)

    if is_integer(room.group_id) do
      Basic.publish(
        state.channel,
        "reactor.messages",
        "group_id:#{room.group_id}",
        :erlang.term_to_binary(Map.put(decoded, :room_id, room.id))
      )
    end

    {:noreply, state}
  end

  # todo test
  @impl true
  def handle_continue({:handle_port, _data, %{handler: "async_error"} = decoded}, state) do
    IO.puts("handle port, async error")
    IO.inspect(decoded)
    {:noreply, state}
  end

  @impl true
  def handle_continue({:handle_port, _data, %{handler: _handler} = _decoded}, state) do
    IO.puts("handle port, unknown")
    # IO.inspect(decoded)
    {:noreply, state}
  end
end
