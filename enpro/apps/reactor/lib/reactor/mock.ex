defmodule Reactor.Mock do
  defp accounts() do
    [
      %{
        username: "user1",
        password: "user1",
        groups: [
          %{
            name: "one",
            visibility: "private"
          },
          %{
            name: "two",
            visibility: "private"
          }
        ],
        services: [
          # %{
          #   name: "service1",
          #   protocol: "irc",
          #   server: "irc.freenode.net",
          #   port: 6667,
          #   nick: "gigachad",
          #   rooms: [
          #     %{
          #       name: "chads",
          #       channel: "chads"
          #     }
          #   ]
          # }
          # %{
          #   name: "service2",
          #   protocol: "irc",
          #   server: "irc.chatjunkies.net",
          #   port: 6667,
          #   nick: "potatos",
          #   rooms: [
          #     %{
          #       name: "chads",
          #       group: "one",
          #       channel: "chads"
          #     },
          #     %{
          #       name: "memes",
          #       group: "two",
          #       channel: "memes"
          #     }
          #   ]
          # },
          %{
            name: "service3",
            protocol: "discord",
            auth_token: "69varygudtoekn420",
            rooms: [
              %{
                name: "chads",
                # group: "one",
                channel_id: "asdf123"
              },
              %{
                name: "memes",
                # group: "two",
                channel_id: "fdsa321"
              }
            ]
          }
          # %{
          #   name: "service4",
          #   protocol: "matrix",
          #   sender: "hideo",
          #   server: "matrix.goettsch.xyz",
          #   access_token: "gr8token",
          #   port: 5000,
          #   rooms: [
          #     %{
          #       name: "memes",
          #       group: "two",
          #       matrix_room_id: "zxcv456"
          #     }
          #   ]
          # }
        ]
      }
      # %{
      #   username: "user2",
      #   password: "user2",
      #   services: [
      #     %{
      #       name: "service1",
      #       protocol: "irc",
      #       server: "irchighway.wtf",
      #       port: 6667,
      #       nick: "brandon"
      #     },
      #     %{
      #       name: "service2",
      #       protocol: "discord",
      #       auth_token: "69varygudtoekn420"
      #     },
      #     %{
      #       name: "service3",
      #       protocol: "discord",
      #       auth_token: "69varygudtoekn420"
      #     }
      #   ]
      # }
    ]
  end

  def create_accounts() do
    alias Reactor.Storage, as: S

    # todo refactor using 'with'?
    for account <- accounts() do
      insert_account_result = S.Account.changeset(%S.Account{}, account) |> S.insert()

      case insert_account_result do
        {:ok, inserted_account} ->
          for group <- account.groups do
            S.Group.changeset(%S.Group{}, Map.put(group, :account_id, inserted_account.id))
            |> S.insert()
          end

          for service <- account.services do
            insert_service_result =
              S.Service.changeset(
                %S.Service{},
                Map.put(service, :account_id, inserted_account.id)
              )
              |> S.insert()

            case insert_service_result do
              {:ok, inserted_service} ->
                s = Map.put(service, :service_id, inserted_service.id)

                case service.protocol do
                  "irc" ->
                    S.ServiceIRC.changeset(%S.ServiceIRC{}, s)

                  "discord" ->
                    S.ServiceDiscord.changeset(%S.ServiceDiscord{}, s)

                  "matrix" ->
                    S.ServiceMatrix.changeset(%S.ServiceMatrix{}, s)
                end
                |> S.insert()

                for room <- service.rooms do
                  insert_room_result =
                    S.Room.changeset(%S.Room{}, Map.put(room, :service_id, inserted_service.id))
                    |> S.insert()

                  case insert_room_result do
                    {:ok, inserted_room} ->
                      r = Map.put(room, :room_id, inserted_room.id)

                      case service.protocol do
                        "irc" ->
                          S.RoomIRC.changeset(%S.RoomIRC{}, r)

                        "discord" ->
                          S.RoomDiscord.changeset(%S.RoomDiscord{}, r)

                        "matrix" ->
                          S.RoomMatrix.changeset(%S.RoomMatrix{}, r)
                      end
                      |> S.insert()

                    _ ->
                      {}
                  end
                end

              _ ->
                {}
            end
          end

        _ ->
          {}
      end
    end
  end
end
