defmodule Reactor.Distribution do
  @behaviour Horde.DistributionStrategy

  def choose_node(child_spec, members) do
    Horde.UniformRandomDistribution.choose_node(child_spec, members)
  end

  def has_quorum?(members) do
    Horde.UniformQuorumDistribution.has_quorum?(members)
  end
end
