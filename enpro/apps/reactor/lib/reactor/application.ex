defmodule Reactor.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    {:ok, ch} = AMQP.Application.get_channel(:ch0)
    :ok = AMQP.Exchange.declare(ch, "reactor.messages", :topic)

    children = [
      Reactor.Storage,
      {Reactor.HordeSupervisor, []},
      {Cluster.Supervisor,
       [Application.get_env(:reactor, ClusterTopologies), [name: Reactor.ClusterSupervisor]]},
      {Reactor.ControlRoom, []}
    ]

    opts = [strategy: :one_for_one, name: Reactor.Supervisor]
    Supervisor.start_link(children, opts)
  end
end

defmodule Reactor.HordeSupervisor do
  use Supervisor

  def start_link(_arg) do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @impl true
  def init(_arg) do
    children = [
      {Horde.Registry, [name: Reactor.HR, keys: :unique, members: :auto]},
      {Horde.DynamicSupervisor,
       [
         name: Reactor.HDS,
         strategy: :one_for_one,
         distribution_strategy: Reactor.Distribution,
         max_restarts: 100_000,
         max_seconds: 1,
         members: :auto
       ]}
    ]

    Supervisor.init(children, strategy: :one_for_all)
  end
end
