defmodule Reactor.Storage.Migrations.MostTables do
  use Ecto.Migration

  def change do
    create table(:account) do
      add :username, :string, null: false
      add :password, :string, null: false
    end
    create unique_index :account, [:username]

    ###

    create table(:service) do
      add :name, :string, null: false
      # todo constrain protocol to be one of irc, discord, matrix, etc.
      add :protocol, :string, null: false
      add :account_id, references("account"), null: false
    end
    create unique_index :service, [:account_id, :name]

    create table(:service_irc) do
      add :server, :string, null: false
      add :port, :integer, null: false
      add :nick, :string, null: false
      add :service_id, references("service"), null: false
    end
    # create unique_index :service, [:account_id, :name]

    create table(:service_discord) do
      add :auth_token, :string, null: false
      add :service_id, references("service"), null: false
    end

    create table(:service_matrix) do
      add :sender, :string, null: false
      add :server, :string, null: false
      add :access_token, :string, null: false
      add :port, :integer, null: false
      add :service_id, references("service"), null: false
    end

    create table(:group) do
      add :name, :string, null: false
      add :visibility, :string, null: false
      add :account_id, references("account"), null: false
    end

    ###

    create table(:room) do
      add :name, :string, null: false
      add :service_id, references("service"), null: false
      add :group_id, references("group"), null: true
    end

    create table(:room_irc) do
      add :channel, :string, null: false
      add :room_id, references("room"), null: false
    end

    create table(:room_discord) do
      add :channel_id, :string, null: false
      add :room_id, references("room"), null: false
    end

    create table(:room_matrix) do
      add :matrix_room_id, :string, null: false
      add :room_id, references("room"), null: false
    end
  end
end
