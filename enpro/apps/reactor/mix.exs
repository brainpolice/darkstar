defmodule Reactor.MixProject do
  use Mix.Project

  def project do
    [
      app: :reactor,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.16",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications:
        case Mix.env() do
          :prod ->
            [
              :logger
            ]

          _ ->
            [
              :logger,
              :runtime_tools,
              :observer,
              :wx
            ]
        end,
      mod: {Reactor.Application, []}
    ]
  end

  defp deps do
    [
      {:libcluster, "~> 3.0"},
      {:horde, "~> 0.9.0"},
      {:amqp, "~> 3.3"},
      {:ecto, "~> 3.10"},
      {:ecto_sqlite3, "~> 0.13"},
      {:ecto_sql, "~> 3.0"},
      {:postgrex, ">= 0.0.0"},
      {:jason, "~> 1.4"},
      {:bcrypt_elixir, "~> 3.0"}
    ]
  end
end
