package main

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand/v2"
	"os"
	"os/signal"
	"time"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	_, cancel := context.WithCancel(context.Background())
	defer cancel()

	f, err := os.OpenFile("/tmp/swann", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}
	log.SetOutput(f)

	// todo remove ctx
	go func() {
		for {
			d := rand.IntN(7-3) + 3

			m := map[string]interface{}{
				"handler":   "new_message",
				"room_ref":  "asdf123",
				"room_name": "room123",
				"author":    "user123",
				"server":    "server123",
				"content":   fmt.Sprintf("random interval %d", d),
			}

			j, err := json.Marshal(m)
			if err != nil {
				log.Println(err)
			}

			time.Sleep(time.Second * time.Duration(d))
			fmt.Printf("%s\n", j)
		}
	}()

	go func() {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			var m map[string]interface{}
			err := json.Unmarshal(scanner.Bytes(), &m)
			if err != nil {
				log.Println(err)
				continue
			}
			log.Println(m)
			// log.Println(m["call_process"].(map[string]interface{})["lmao"]) // todo handle panic

			switch m["handler"] {
			case "mmmboy": // sync
				log.Printf("handler, mmmboy")

				r := map[string]interface{}{
					"handler": "reply",
					"ref":     m["ref"],
					"status":  "ok",
				}

				j, err := json.Marshal(r)
				if err != nil {
					log.Println(err)
				}

				time.Sleep(time.Second * 10)
				fmt.Printf("%s\n", j)

			case "register_rooms": // sync
				log.Printf("handler, register rooms")

				r := map[string]interface{}{
					"handler": "reply",
					"ref":     m["ref"],
					"status":  "ok",
				}

				j, err := json.Marshal(r)
				if err != nil {
					log.Println(err)
				}

				fmt.Printf("%s\n", j)

			case "register_rooms_async": // async
				log.Printf("handler, register rooms async")

			case "new_message": // async
				log.Printf("handler, new message")

			default:
				log.Printf("unknown handler %s", m["handler"])
			}
		}
	}()

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt)
	<-sigc

	return nil
}
