package main

import (
	"bufio"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"

	"github.com/bwmarrin/discordgo"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

var AuthToken = flag.String("auth_token", "", "auth token")

func run() error {
	flag.Parse()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	f, err := os.OpenFile("/tmp/campbell", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return err
	}
	log.SetOutput(f)
	log.SetPrefix("campbell ")

	campbell, err := NewCampbell(*AuthToken)
	if err != nil {
		return err
	}
	defer campbell.Close()

	go campbell.Start(ctx)

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt)
	<-sigc

	return nil
}

type Campbell struct {
	Session   *discordgo.Session
	portInput chan map[string]any
}

func NewCampbell(authToken string) (*Campbell, error) {

	session, err := discordgo.New("Bot " + authToken)
	if err != nil {
		return nil, err
	}

	session.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if m.Author.ID == s.State.User.ID {
			return
		}

		msg := map[string]any{
			"handler":  "new_message",
			"room_ref": m.ChannelID,
			// "room_name": "room123",
			"author": m.Author.Username,
			// todo change server to origin
			"server":  "Discord",
			"content": m.Content,
		}

		j, err := json.Marshal(msg)
		if err != nil {
			log.Println(err)
		}

		fmt.Printf("%s\n", j)
	})

	session.Identify.Intents = discordgo.IntentGuildMessages

	portInput := make(chan map[string]any)

	campbell := Campbell{
		Session:   session,
		portInput: portInput,
	}

	return &campbell, nil
}

func (x *Campbell) Start(ctx context.Context) {
	err := x.Session.Open()
	if err != nil {
		log.Printf("todo handle error")
		return
	}

	go x.scan()

	for {
		select {
		case <-ctx.Done():
			return

		case msg := <-x.portInput:
			switch msg["handler"] {
			case "mmmboy": // sync
				x.mmmboy(msg)

			case "register_rooms": // sync
				x.register_rooms(msg)

			case "register_rooms_async": // async
				log.Printf("handler, register rooms async")

			case "new_message": // async
				x.new_message(msg)

			default:
				log.Printf("unknown handler %s", msg["handler"])
			}
		}
	}
}

func (x *Campbell) scan() {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		var m map[string]any
		err := json.Unmarshal(scanner.Bytes(), &m)
		if err != nil {
			log.Println(err)
			continue
		}
		x.portInput <- m
	}
}

func syncHandler(msg map[string]any, f func(msg map[string]any) error) {
	r := map[string]any{
		"handler": "reply",
		"ref":     msg["ref"],
	}

	if err := f(msg); err != nil {
		r["status"] = "error"
		r["error"] = err.Error()
	} else {
		r["status"] = "ok"
	}

	j, err := json.Marshal(r)
	if err != nil {
		log.Println(err)
	}

	fmt.Printf("%s\n", j)
}

func (x *Campbell) mmmboy(msg map[string]any) {
	syncHandler(msg, func(msg map[string]any) error {
		log.Printf("handler, mmmboy")
		return nil
	})
}

func (x *Campbell) register_rooms(msg map[string]any) {
	syncHandler(msg, func(msg map[string]any) error {
		log.Printf("handler, register rooms")
		return nil
	})
}

type NewMessage struct {
	RoomRefs []string `json:"room_refs"`
	Content  string   `json:"content"`
}

func ToReactorMessage(msg map[string]any) (any, error) {
	j, err := json.Marshal(msg)
	if err != nil {
		log.Println(err)
	}

	switch msg["handler"] {
	case "new_message":
		var newMessage NewMessage

		err := json.Unmarshal(j, &newMessage)
		if err != nil {
			return nil, err
		}

		return newMessage, nil

	default:
		return nil, nil
	}
}

func (x *Campbell) new_message(msg map[string]any) {
	log.Printf("handler, new message")

	foo, _ := ToReactorMessage(msg)
	bar := foo.(NewMessage)
	log.Println(bar.RoomRefs)
	log.Println(bar.Content)

	room_refs := msg["room_refs"].([]interface{})
	for _, channel_id_iface := range room_refs {
		channel_id := channel_id_iface.(string)
		// todo format message
		x.Session.ChannelMessageSend(channel_id, msg["content"].(string))
	}
}

func (x *Campbell) Close() {
	x.Session.Close()
}
